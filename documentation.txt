Customer Tax Exempt Modification to Zen Cart

USE AT YOUR OWN RISK.

ALWAYS BACKUP YOUR FILES AND DATABASE BEFORE MAKING ANY CHANGES TO YOUR STORE.

MODIFIED FILE(S):
===============
/admin/customers.php
/includes/functions/functions_taxes.php
**Search the above files for "customer tax exempt edit" (no quotes) to find the modified sections.

NEW FILE(S):
============
/admin/includes/languages/english/extra_definitions/customer_tax_exempt.php

INSTALL:
========
If you have made any changes or have installed addons that made changes to the
MODIFIED files listed above you will need to edited these files manually.

1. Run the SQL patch with the SQL tool in Zen Cart's Admin
2. Upload the contents of �includes� into your store's includes directory (NOT the admin includes directory)
3. Upload the contents of �admin� into your admin directory

USE:
====
From the customers' page, input the appropriate tax description to make that tax exempt for the current customer.
An example is given displaying the actual tax descriptions used in YOUR store.