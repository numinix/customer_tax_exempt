<?php
  define('HEADING_TITLE', 'Customer\'s Tax Exempt Report');
  define('TABLE_HEADING_CUSTOMERS_NAME', 'Customer\'s Name');
  define('TABLE_HEADING_COMPANY_NAME', 'Company Name');
  define('TABLE_HEADING_CUSTOMERS_TAX_EXEMPT_STATUS', 'Tax Exempt Status');