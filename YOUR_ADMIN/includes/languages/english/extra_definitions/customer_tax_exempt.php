<?php
//  Title: Customers Tax Exempt
//  Author: Jeff Lew
//  Contact: jlew@numinix.com
//  Last Update: 24/07/2007 by Jeff Lew
//  Organization: Numinix Technology <http://www.numinix.com>


define('ENTRY_TAX_EXEMPT', 'Customer Tax Exempt');
define('NOTES_TAX_EXEMPT', '&nbsp; &nbsp;Enter exempt taxes\' description or "ALL"');
define('TAX_EXEMPT_EXAMPLE', '(Example: ');
?>