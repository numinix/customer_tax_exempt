<?php
/**
 * @package admin
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: music_type_filenames.php 3001 2006-02-09 21:45:06Z wilt $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}
  define('FILENAME_STATS_CUSTOMERS_TAX_EXEMPT', 'stats_customers_tax_exempt');
  define('BOX_STATS_CUSTOMERS_TAX_EXEMPT', 'Customer\'s Tax Exempt');