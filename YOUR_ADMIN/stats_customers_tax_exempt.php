<?php
  require('includes/application_top.php');
  $customers_tax_exempt = $db->Execute("SELECT c.customers_id, c.customers_tax_exempt, c.customers_firstname, c.customers_lastname, ab.entry_company FROM " . TABLE_CUSTOMERS . " c LEFT JOIN " . TABLE_ADDRESS_BOOK . " ab ON (ab.address_book_id = c.customers_default_address_id) WHERE c.customers_tax_exempt IS NOT NULL AND c.customers_tax_exempt <> '' ORDER BY c.customers_lastname ASC;");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/cssjsmenuhover.css" media="all" id="hoverJS">
<script language="javascript" src="includes/menu.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script type="text/javascript">
  <!--
  function init()
  {
    cssjsmenu('navbar');
    if (document.getElementById)
    {
      var kill = document.getElementById('hoverJS');
      kill.disabled = true;
    }
  if (typeof _editor_url == "string") HTMLArea.replaceAll();
  }
  // -->
</script>
<style type="text/css">
  .container {margin: 40px;}
  table#customersTaxExemptReport th, table#customersTaxExemptReport td {padding: 5px;}
</style>
<?php if (HTML_EDITOR_PREFERENCE=="FCKEDITOR") require(DIR_WS_INCLUDES.'fckeditor.php'); ?>
<?php if (HTML_EDITOR_PREFERENCE=="HTMLAREA")  require(DIR_WS_INCLUDES.'htmlarea.php'); ?>
</head>

<body onLoad="init()">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
  <div class="container">
    <h1><?php echo HEADING_TITLE; ?></h1>
    <?php
      if ($customers_tax_exempt->RecordCount() > 0) {
        echo '<table id="customersTaxExemptReport" border="0">' . "\n";
        echo '  <tr>' . "\n";
        echo '    <th>' . TABLE_HEADING_CUSTOMERS_NAME . '</th><th>' . TABLE_HEADING_COMPANY_NAME . '</th><th>' . TABLE_HEADING_CUSTOMERS_TAX_EXEMPT_STATUS . '</th>' . "\n";
        echo '  </tr>' . "\n";
        while (!$customers_tax_exempt->EOF) {
          echo '  <tr>' . "\n";
          echo '    <td><a href="' . zen_href_link(FILENAME_CUSTOMERS, 'cID=' . $customers_tax_exempt->fields['customers_id'] . '&action=edit') . '">' . $customers_tax_exempt->fields['customers_lastname'] . ', ' . $customers_tax_exempt->fields['customers_firstname'] . '</a></td><td>' . $customers_tax_exempt->fields['entry_company'] . '</td><td>' . $customers_tax_exempt->fields['customers_tax_exempt'] . '</td>' . "\n";
          echo '  </tr>' . "\n";
          $customers_tax_exempt->MoveNext();
        }
        echo '</table>' . "\n";
      } else {
        echo '<p>No Tax Exempt Customers.</p>' . "\n";
      } 
    ?>
  </div>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>